# Group Ping Bot

## Contents

* [Group Ping Bot](#group-ping-bot)
   * [Contents](#contents)
   * [Overview](#overview)
   * [@group VS. /ping group](#group-vs-ping-group)
   * [Supported commands:](#supported-commands)
   * [Hosting the Bot](#hosting-the-bot)
      * [Dependencies](#dependencies)
      * [Config](#config)
      * [How to test](#how-to-test)


## Overview

This bot allows you to add members of a group chat into smaller "groups" (similar to roles in, for example, Discord) which can then be pinged.

A simple use-case would be to add everyone to the group "all" and then pinging the whole group with "`@all`" of with "`/ping all`".  

Another example:
```
/create_groups programmers
/join programmers
"Hey, @programmers in this group, can you help me write this bot?"
```

## @group VS. /ping group
The `/ping group1 group2 ...` command, like any other command, has to be in its own message but takes several groups as arguments.  
The `@group`-syntax can be used anywhere in a message but can only reference one group per "@", e.g. "Does anyone want to play `@game1` or `@game2` with me tonight?"

To enable the simpler `@group` syntax, your bot must be able to read all messages in telegram-groups, not just the ones directed at the bot via replies or commands.
For this, you have to disable [Privacy Mode](https://core.telegram.org/bots#privacy-mode).  
**IMPORTANT:** In order for this change to apply, you need to remove your bot from the groups and re-add it!

## Supported commands:
__Create new groups__  
`/create_groups <group1> <group2> ...`

__Delete groups__  
`/delete_groups <group1> <group2> ...`

__List all groups in this chat__  
`/groups`  
`/list_groups`

__List all members of a specified group__  
`/members <group>`  
`/list_members <group>`

__List the members of a group along with their unique User ID__  
`/members_verbose <group>`  
`/list_members_verbose <group>`

__List all groups of which you are a member__  
`/my_groups`

__Join groups__  
`/join <group1> <group2> ...`

__Add another user to groups__  
`/add <user_id> <group1> <group2> ...`  
`/join_id <user_id> <group1> <group2> ...`

__Leave groups__   
`/leave <group1> <group2> ...`

__Remove another user from groups__  
`/remove <user_id> <group1> <group2> ...`  
`/leave_id <user_id> <group1> <group2> ...`

__Ping all users in the specified group__  
`/ping <group> <message...>`

## Hosting the Bot

Make sure you have a database running and enter the URL in the config explained below.
View the [SQLAlchemy documentation](https://docs.sqlalchemy.org/en/14/core/engines.html) to find a list of supported databases and how the URL should be formatted.
Please refer to your database's documentation for instructions on how to create a database.


### Dependencies
```
pip install -r requirements.txt
```
You will also need to install a DBAPI package such as `pymysql` if you are using MySQL or MariaDB.
Running the bot should give you an error message which will let you know if something is missing.

### Config
The config resides in `config.yaml` in the top level of the directory.

```yaml
token: "YourBotTokenGoesHere15373751"

# Everything below is for testing.
api_id: #can be found here: https://my.telegram.org/apps
api_hash: #can be found here: https://my.telegram.org/apps
group_invite_link: #the group invite link of your test group
telephone: #the telephone number of your telegram account
```


### How to test
1. Create your Test Bot with BotFather
2. Create a Test Group in the Telegram UI
3. Invite your Test Bot and start the bot
4. In the source dir write: ```pytest tests/*```

   
