import random
from time import sleep

random_number = random.getrandbits(12)


def test_ping_group(telegram_client, client_entity, user_name):
    telegram_client.send_message(client_entity, f"/create_groups {random_number}")
    telegram_client.send_message(client_entity, f"/join {random_number}")
    telegram_client.send_message(client_entity, f"/ping {random_number}")
    sleep(0.2)
    last_message = telegram_client.get_messages(client_entity)[0].message
    assert user_name.lower() in last_message.lower()
    telegram_client.send_message(client_entity, f"/leave {random_number}")
    telegram_client.send_message(client_entity, f"/delete_groups {random_number}")
