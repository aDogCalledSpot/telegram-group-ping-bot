import pytest
import yaml
from telethon.sync import TelegramClient

with open("config.yaml") as config_file:
    config = yaml.safe_load(config_file)

client = TelegramClient(None, config["api_id"], config["api_hash"])
"""
    Yes we sadly need to use a pre-existing user account and cannot use
    the bot token. CI/CD hence seems impossible.
"""
client.start(phone=config["telephone"])


@pytest.fixture
def telegram_client():
    return client


@pytest.fixture
def client_entity():
    return client.get_entity(config["group_invite_link"])


@pytest.fixture
def user_name():
    return client.get_me().first_name
