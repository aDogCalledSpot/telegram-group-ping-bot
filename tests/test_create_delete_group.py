import random
from time import sleep

random_number = random.getrandbits(12)


def test_create_group(telegram_client, client_entity):
    telegram_client.send_message(client_entity, f"/create_groups {random_number}")
    telegram_client.send_message(client_entity, "/groups")
    sleep(0.2)
    last_message = telegram_client.get_messages(client_entity)[0].message
    assert str(random_number) in last_message


def test_delete_group(telegram_client, client_entity):
    telegram_client.send_message(client_entity, f"/delete_groups {random_number}")
    telegram_client.send_message(client_entity, "/groups")
    sleep(0.2)
    last_message = telegram_client.get_messages(client_entity)[0].message
    assert str(random_number) not in last_message
