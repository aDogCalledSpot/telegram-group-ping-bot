from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from telegram.error import BadRequest
from time import sleep
import logging
import yaml
import threading
from sqlalchemy import (
    Column,
    Integer,
    BigInteger,
    String,
    ForeignKey,
    Table,
    create_engine
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import backref, relationship, sessionmaker, scoped_session
import re

Base = declarative_base()


class Chat(Base):
    __tablename__ = "chats"
    id = Column(BigInteger, primary_key=True, nullable=False)
    groups = relationship("Group")


user_group_association = Table('user_group_association', Base.metadata,
    Column('user', ForeignKey('users.id')),
    Column('group', ForeignKey('groups.id'))
)

class Group(Base):
    __tablename__ = "groups"
    id = Column(Integer, primary_key=True, nullable=False)
    chat = Column(BigInteger, ForeignKey('chats.id'))
    name = Column(String(64), nullable=False)
    users = relationship("User", secondary=user_group_association,
                         back_populates="groups")


class User(Base):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True, nullable=False)
    groups = relationship("Group", secondary=user_group_association,
                          back_populates="users")


def start(update, context):
    if update.effective_chat.id < 0:
        # Group
        text = "Hi, I'm the Group Ping Bot. You can get started by creating a group with /create_groups. Check out more of my commands with /help."
    else:
        # Single User
        text = "Hi, I'm the Group Ping Bot. You should probably add me to a group."
    context.bot.send_message(chat_id=update.effective_chat.id, text=text)


def is_group_name_invalid(name):
    # Search for @, /, \, # and potentially markdown-triggering characters
    violations = re.findall(r'(@|#|\/|\\|\(|\)|\[|\]|\*\*|__|~~|`)', name)
    return violations if len(violations) > 0 else None


def new_group(update, context):
    chat_id = update.effective_chat.id

    # Split the first word away
    group_names = update.message.text.split(' ')[1:]
    if len(group_names) == 0:
        text = "Missing argument. Specify the names of the groups (e.g. \"create_groups myGroup anotherGroup\")"
        context.bot.send_message(chat_id=chat_id, text=text)
        return

    with DBSession() as session:
        if session.query(Chat)\
            .filter_by(id=chat_id).first() is None:
            session.add(Chat(id=chat_id))

        created_groups = []
        invalid_characters = []
        groups_existed = False

        for group_name in group_names:
            if session.query(Group).filter_by(name=group_name, chat=chat_id).first() is not None:
                groups_existed = True
            else:
                # New group, check name first
                _invalid = is_group_name_invalid(group_name)
                if _invalid:
                    invalid_characters.extend(_invalid)
                else:
                    # Group doesn't exist yet and name is fine
                    db_group = Group(chat=chat_id, name=group_name)
                    session.add(db_group)
                    created_groups.append(group_name)
        session.commit()

    if len(created_groups) == 0:
        text = "No new groups were created.\n"
    else:
        text = "Created the following groups:\n\n"
        for group_name in sorted(created_groups, key=str.lower):
            text += f"{group_name}\n"

    if groups_existed:
        text += f"\n\nSome groups were skipped because they already exist."
    if len(invalid_characters) > 0:
        text += f"\n\nSome groups were skipped because of faulty names. The following invalid characters were found: " \
                f'{", ".join(list(dict.fromkeys(invalid_characters)))}'

    context.bot.send_message(chat_id=update.effective_chat.id, text=text)


def delete_group(update, context):
    chat_id = update.effective_chat.id
    with DBSession() as session:
        db_chat = session.query(Chat).filter_by(id=chat_id).first()
        if db_chat is None or len(db_chat.groups) == 0:
            text = "This chat hasn't registered any groups yet. You can create some with /create_groups."
            context.bot.send_message(chat_id=chat_id, text=text)
            return

    group_names = update.message.text.split(' ')[1:]
    if len(group_names) == 0:
        text = "Missing argument. Specify the names of the groups (e.g \"/delete_groups mygroup anothergroup\")."
        context.bot.send_message(chat_id=chat_id, text=text)
        return

    deleted_groups = []
    skipped = False

    with DBSession() as session:
        for group in group_names:
            db_group = session.query(Group).filter_by(name=group, chat=chat_id).first()
            if db_group is None:
                skipped = True
                continue
            session.delete(db_group)
            deleted_groups.append(group)
        session.commit()

    if len(deleted_groups) == 0:
        text = "No groups were deleted. Make sure the groups you are deleting are empty."
        context.bot.send_message(chat_id=chat_id, text=text)
        return

    text = "The following groups were removed:\n\n"
    for group in sorted(deleted_groups, key=str.lower):
        text += f"{group}\n"

    if skipped:
        text += "\nSome groups were skipped. Make sure you spelled them correctly and that the groups are empty."
    context.bot.send_message(chat_id=chat_id, text=text)


def list_groups(update, context, verbose=False):
    chat_id = update.effective_chat.id
    with DBSession() as session:
        db_chat = session.query(Chat).filter_by(id=chat_id).first()
        if db_chat is None or len(db_chat.groups) == 0:
            text = "It seems you don't have any groups for this chat. Try creating one with /create_groups."
            context.bot.send_message(chat_id=chat_id, text=text)
            return

        text = "The current groups exist in this chat:\n\n"

        for group in sorted(db_chat.groups, key=lambda group: group.name.lower()):
            text += group.name
            if verbose:
                text += f" ({len(group.users)} member"
                text += "s)" if len(group.users) != 1 else ")"
            text += "\n"

        text += "\nYou can view the participants in a group with /members"

    context.bot.send_message(chat_id=chat_id, text=text)


def list_groups_verbose(update, context):
    list_groups(update, context, True)


def list_groups_short(update, context):
    list_groups(update, context)


def list_members(update, context, verbose):
    chat_id = update.effective_chat.id
    # Split the first word away
    group_name = update.message.text.split(' ')[1:]
    if len(group_name) == 0:
        command = "/members_verbose" if verbose else "/members"
        text = f"Missing argument. Specify the name of the group (e.g. \"{command} mygroup\")"
        context.bot.send_message(chat_id=chat_id, text=text)
        return
    elif len(group_name) > 1:
        text = "Listing multiple groups currently not supported."
        context.bot.send_message(chat_id=chat_id, text=text)
        return


    with DBSession() as session:
        db_chat = session.query(Chat).filter_by(id=chat_id).first()
        if db_chat is None or len(db_chat.groups) == 0:
            text = "This chat hasn't registered any groups yet. You can create some with /create_groups."
            context.bot.send_message(chat_id=chat_id, text=text)
            return

        group_name = group_name[0]
        db_group = session.query(Group).filter_by(name=group_name, chat=chat_id).first()
        if db_group is None:
            text = "No group with this name exists for this chat. You can list existing groups with /groups."
            context.bot.send_message(chat_id=chat_id, text=text)
            return

        if len(db_group.users) == 0:
            text = f"The group \"{group_name}\" is currently empty."
            context.bot.send_message(chat_id=chat_id, text=text)
            return

        text = f"The group \"{group_name}\" has the following members:\n\n"
        members = []
        for member in db_group.users:
            try:
                members.append((context.bot.get_chat_member(chat_id, member.id).user.first_name, member.id))
            except BadRequest:
                # User left chat
                # Remove from all groups
                member.groups = [group for group in member.groups if group != chat_id]
                session.commit()

        for member in sorted(members, key=lambda member: member[0].lower()):
            text += member[0]
            if verbose:
                text += f" ({member[1]})"
            text += "\n"

    context.bot.send_message(chat_id=chat_id, text=text)
    return


def list_members_short(update, context):
    list_members(update, context, False)


def list_members_verbose(update, context):
    list_members(update, context, True)


def add_member(update, context, user_id, group_names):
    chat_id = update.effective_chat.id
    with DBSession() as session:
        db_chat = session.query(Chat).filter_by(id=chat_id).first()
        if db_chat is None or len(db_chat.groups) == 0:
            text = "This chat hasn't registered any groups yet. You can create some with /create_groups."
            context.bot.send_message(chat_id=chat_id, text=text)
            return

        added_groups = []
        skipped = False
        db_user = session.query(User).filter_by(id=user_id).first()
        if db_user is None:
            db_user = User(id=user_id)
            session.add(db_user)
        for group in group_names:
            db_group = session.query(Group).filter_by(name=group, chat=chat_id).first()
            if db_group is None or db_user in db_group.users:
                skipped = True
                continue

            db_user.groups.append(db_group)
            added_groups.append(group)
        session.commit()

    if len(added_groups) == 0:
        text = "User wasn't added to any groups. Make sure you spelled the groups correctly and that the user isn't already a member."
        context.bot.send_message(chat_id=chat_id, text=text)
        return

    name = context.bot.get_chat_member(chat_id, user_id).user.first_name
    text = f"{name} has been added to the following groups:\n\n"
    for group in sorted(added_groups, key=str.lower):
        text += f"{group}\n"

    if skipped:
        text += "\nSome groups were skipped. Make sure you spelled them correctly and that the user isn't already a member."
    context.bot.send_message(chat_id=chat_id, text=text)


def add_member_current(update, context):
    groups = update.message.text.split(' ')[1:]

    if len(groups) == 0:
        text = "No arguments specified. Command requires a list of groups (e.g. /join  mygroup anothergroup)."
        context.bot.send_message(chat_id=update.effective_chat.id, text=text)
        return

    add_member(update, context, update.effective_user.id, groups)


def add_member_with_id(update, context):
    args = update.message.text.split(' ')[1:]

    if len(args) == 0:
        text = "No arguments specified. Command requires a user ID followed by a list of groups (e.g. /add 132575 mygroup anothergroup)."
        context.bot.send_message(chat_id=update.effective_chat.id, text=text)
        return

    if len(args) == 1:
        text = "Missing group name(s). Command requires a user ID followed by a list of groups (e.g. /add 132575 mygroup anothergroup)."
        context.bot.send_message(chat_id=update.effective_chat.id, text=text)
        return

    user = cast_user_id_to_int(update, context, args[0])
    if not user:
        return
    add_member(update, context, user, args[1:])


def cast_user_id_to_int(update, context, user_id):
    try:
        user = int(user_id)
        context.bot.get_chat_member(update.effective_chat.id, user)
        return user
    except ValueError:
        text = "Specified user must be the unique ID of the user."
        context.bot.send_message(chat_id=update.effective_chat.id, text=text)
        return None
    except BadRequest:
        text = "Specified user not in this chat."
        context.bot.send_message(chat_id=update.effective_chat.id, text=text)
        return None


def remove_member(update, context, user_id, group_names):
    removed_groups = []
    skipped = False

    chat_id = update.effective_chat.id
    with DBSession() as session:
        db_user = session.query(User).filter_by(id=user_id).first()
        for group in group_names:
            db_group = session.query(Group).filter_by(name=group, chat=chat_id).first()
            if db_group is None or db_user not in db_group.users:
                skipped = True
                continue

            db_user.groups.remove(db_group)
            removed_groups.append(group)
        session.commit()

    if len(removed_groups) == 0:
        text = "User wasn't removed from any groups. Make sure you spelled them correctly and that the user is a member."
        context.bot.send_message(chat_id=chat_id, text=text)
        return

    name = context.bot.get_chat_member(chat_id, user_id).user.first_name
    text = f"{name} removed from groups:\n\n"
    for group in sorted(removed_groups, key=str.lower):
        text += f"{group}\n"

    if skipped:
        text += "\nSome groups were skipped. Make sure you spelled them correctly and that the user is a member."
    context.bot.send_message(chat_id=chat_id, text=text)


def remove_member_current(update, context):
    groups = update.message.text.split(' ')[1:]

    if len(groups) == 0:
        text = "No arguments specified. Command requires a list of groups (e.g. /leave  mygroup anothergroup)."
        context.bot.send_message(chat_id=update.effective_chat.id, text=text)
        return

    remove_member(update, context, update.effective_user.id, groups)


def remove_member_with_id(update, context):
    args = update.message.text.split(' ')[1:]

    if len(args) == 0:
        text = "No arguments specified. Command requires a user ID followed by a list of groups (e.g. /remove 132575 mygroup anothergroup)."
        context.bot.send_message(chat_id=update.effective_chat.id, text=text)
        return

    if len(args) == 1:
        text = "Missing group name(s). Command requires a user ID followed by a list of groups (e.g. /remove 132575 mygroup anothergroup)."
        context.bot.send_message(chat_id=update.effective_chat.id, text=text)
        return

    user = cast_user_id_to_int(update, context, args[0])
    if not user:
        return
    remove_member(update, context, user, args[1:])


def ping(update, context):
    chat_id = update.effective_chat.id

    # Split the first word away
    group_names = update.message.text.split(' ')[1:]
    if len(group_names) == 0:
        text = "Missing argument. Specify the names of the groups (e.g. '/ping myGroup anotherGroup')."
        context.bot.send_message(chat_id=chat_id, text=text)
        return

    with DBSession() as session:
        db_chat = session.query(Chat).filter_by(id=chat_id).first()
        if db_chat is None or len(db_chat.groups) == 0:
            text = "No groups exist for this chat yet. Try creating some with /create_groups."
            context.bot.send_message(chat_id=chat_id, text=text)
            return

        invalid_grp = []
        for group_name in group_names:
            is_valid = False
            for db_group in db_chat.groups:
                if db_group.name == group_name:
                    is_valid = True
                    break
            if not is_valid:
                invalid_grp.append(group_name)

    if len(invalid_grp) > 0:
        invalid_msg = f'The following group names were invalid and will not be pinged: ' + ', '.join(sorted(invalid_grp, key=str.lower))
        context.bot.send_message(chat_id=chat_id, text=invalid_msg)
        group_names = [item for item in group_names if item not in invalid_grp]

    _ping(update, context, group_names)


def _ping(update, context, group_names):
    chat_id = update.effective_chat.id

    markdown_friendly_group_names = [s.replace("_", "\_") for s in group_names]
    text = f'Pinging groups: {", ".join(sorted(markdown_friendly_group_names, key=str.lower))}\n'

    with DBSession() as session:
        ping_names = []
        for group_name in group_names:
            db_group = session.query(Group).filter_by(name=group_name, chat=chat_id).first()
            for db_member in db_group.users:
                try:
                    name = context.bot.get_chat_member(chat_id, db_member.id).user.first_name
                    ping_names.append(f"[{name}](tg://user?id={db_member.id})\n")
                except BadRequest:
                    # User left chat
                    # Remove from all groups
                    db_member.groups = [group for group in db_member.groups if group != chat_id]
                    session.commit()

    for name in sorted(set(ping_names), key=str.lower):
        text += name

    context.bot.send_message(chat_id=chat_id, text=text, parse_mode='Markdown',
                             reply_to_message_id=update.message.message_id)


def prune(update, context):
    chat_id = update.effective_chat.id
    user_id = update.effective_user.id
    message_id = update.message.message_id

    # This command needs to be done by an admin (or private chat)
    user_status = context.bot.get_chat_member(chat_id, user_id).status
    if chat_id < 0 and user_status not in ["creator", "administrator"]:
        text = "Prune can only be used by admins."
        context.bot.send_message(chat_id=chat_id, text=text, reply_to_message_id=message_id)
        return

    args = update.message.text.split(' ')[1:]
    if len(args) > 1:
        text = "Please only enter one number at most as an argument"
        context.bot.send_message(chat_id=chat_id, text=text, reply_to_message_id=message_id)
        return

    if len(args) == 0:
        prune_threshold = 0
    else:
        try:
            prune_threshold = int(args[0])
        except ValueError:
            text = "Please only enter positive whole numbers (amount of members)"
            context.bot.send_message(chat_id=chat_id, text=text, reply_to_message_id=message_id)
            return

    with DBSession() as session:
        db_chat = session.query(Chat).filter_by(id=chat_id).first()
        if db_chat is None:
            db_chat = Chat(id=chat_id)
            session.add(db_chat)

        old_number_of_groups = len(db_chat.groups)
        db_chat.groups = [x for x in db_chat.groups if len(x.users) > prune_threshold]
        session.commit()
        diff = old_number_of_groups - len(db_chat.groups)
        text = f"Pruned {diff} group"
        text += "s." if diff != 1 else "."

    context.bot.send_message(chat_id=chat_id, text=text, reply_to_message_id=message_id)


def help_message(update, context):
    text = """
*You can use the following commands:*

_Create new groups_
/create\_groups <group1> <group2> ...

_Delete groups_
/delete\_groups <group1> <group2> ...

_List all groups in this chat_
/groups

_List all members of a specified group_
/members <group>

_List all groups of which you are a member_
/my\_groups

_Join groups_
/join <group1> <group2> ...

_Leave groups_
/leave <group1> <group2> ...

_Ping all users in the specified group_
/ping <group> <message...>

This is only a short list. For the full list of (advanced) commands, use /help\_verbose.
    """
    context.bot.send_message(update.effective_chat.id, text=text, parse_mode='Markdown')


def help_message_verbose(update, context):
    text = """
This is the full list of commands, including aliases for backwards-compatibility.
Also note that the only feasible way to get another user's ID is by using a verbose listing command on a group they are already in.

_Create new groups_
/create\_groups <group1> <group2> ...

_Delete groups_
/delete\_groups <group1> <group2> ...

_Prune groups with less than `threshold` members (defaults to 0)_
/prune\_groups [threshold]

_List all groups in this chat_
/groups
/list\_groups

_List all groups in this chat with their respective amount of members_
/groups\_verbose
/list\_groups\_verbose

_List all members of a specified group_
/members <group>
/list\_members <group>

_List the members of a group along with their unique User ID_
/members\_verbose <group>
/list\_members\_verbose <group>

_List all groups of which you are a member_
/my\_groups

_Join groups_
/join <group1> <group2> ...

_Add another user to groups_
/add <user\_id> <group1> <group2> ...
/join\_id <user\_id> <group1> <group2> ...

_Leave groups_
/leave <group1> <group2> ...

_Remove another user from groups_
/remove <user\_id> <group1> <group2> ...
/leave\_id <user\_id> <group1> <group2> ...

_Ping all users in the specified group_
/ping <group> <message...>
    """
    context.bot.send_message(update.effective_chat.id, text=text, parse_mode='Markdown')


def groups_of_which_user_is_member(user_id, chat_id, message_id, bot):
    with DBSession() as session:
        db_chat = session.query(Chat).filter_by(id=chat_id).first()
        if db_chat is None or len(db_chat.groups) == 0:
            text = "No groups exist for this chat yet. Try creating some with /create_groups."
            bot.send_message(chat_id=chat_id, text=text)
            return

        name = bot.get_chat_member(chat_id, user_id).user.first_name
        text = f"{name} is a member of the following groups:\n\n"
        db_user = session.query(User).filter_by(id=user_id).first()
        joined_groups = []
        if db_user is not None:
            for group in db_user.groups:
                if group.chat == chat_id:
                    joined_groups.append(f"{group.name}\n")

            for group in sorted(joined_groups, key=str.lower):
                text += group

    if len(joined_groups) == 0:
        text = "You aren't a member of any groups. Join some with /join."
    bot.send_message(chat_id=chat_id, text=text, reply_to_message_id=message_id)


def groups_of_which_user_themselves_is_member(update, context):
    chat_id = update.effective_chat.id
    user_id = update.effective_user.id
    message_id = update.message.message_id
    groups_of_which_user_is_member(user_id, chat_id, message_id, context.bot)


def groups_of_which_other_user_is_member(update, context):
    chat_id = update.effective_chat.id
    message_id = update.message.message_id

    user_ids = update.message.text.split(' ')[1:]
    if len(user_ids) == 0:
        text = "Missing argument. Specify the Telegram user IDs of the people who's group you want to see (e.g. /reveal_groups 12345678)"
        context.bot.send_message(chat_id=chat_id, text=text)
        return
    if len(user_ids) > 1:
        text = "Please specify only one user id."
        context.bot.send_message(chat_id=chat_id, text=text)
        return
    groups_of_which_user_is_member(user_ids[0], chat_id, message_id, context.bot)


def at_ping(update, context):
    # Check if the current chat has any entries at all
    chat_id = update.effective_chat.id
    with DBSession() as session:
        db_chat = session.query(Chat).filter_by(id=chat_id).first()
        if db_chat is None or len(db_chat.groups) == 0:
            return

        msg = update.message.text
        # Beginning of string or whitespace then an @ and then alphanumeric and _
        mentioned_names = re.findall(r'(?:^|\s)@(\w+)\b', msg)

        # Ping any relevant mentions
        relevant = []
        for name in mentioned_names:
            for db_group in db_chat.groups:
                if db_group.name == name:
                    relevant.append(name)
                    break

    if len(relevant) > 0:
        _ping(update, context, relevant)


if __name__ == "__main__":
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                        level=logging.INFO)

    with open('config.yaml') as config_file:
        config = yaml.safe_load(config_file)
        updater = Updater(token=config['token'], use_context=True)
        db_engine = create_engine(config["db_host"], pool_pre_ping=True)
        session_factory = sessionmaker(bind=db_engine)
        DBSession = scoped_session(session_factory)
        Base.metadata.create_all(db_engine)

    # This is just for backwards compatibility with the old backups
    # Please make proper backups with your database of choice
    # try:
    #     with open("groups.backup") as file:
    #         GROUPS = yaml.safe_load(file)
    #         with DBSession() as session:
    #             for chat in GROUPS:
    #                 db_chat = Chat(id=chat)
    #                 session.add(db_chat)
    #                 for group in GROUPS[chat]:
    #                     db_group = Group(chat=chat, name=group)
    #                     session.add(db_group)
    #                     for user in GROUPS[chat][group]:
    #                         db_user = session.query(User).filter_by(id=user).first()
    #                         if db_user is None:
    #                             db_user = User(id=user, groups=[])
    #                             session.add(db_user)
    #                         db_user.groups.append(db_group)
    #             session.commit()

    # except FileNotFoundError:
    #     pass

    updater.dispatcher.add_handler(CommandHandler('start', start, filters=~Filters.update.edited_message))
    updater.dispatcher.add_handler(CommandHandler('create_groups', new_group, filters=~Filters.update.edited_message))
    updater.dispatcher.add_handler(CommandHandler('delete_groups', delete_group, filters=~Filters.update.edited_message))
    updater.dispatcher.add_handler(CommandHandler('list_groups', list_groups_short, filters=~Filters.update.edited_message))
    updater.dispatcher.add_handler(CommandHandler('list_groups_verbose', list_groups_verbose, filters=~Filters.update.edited_message))
    updater.dispatcher.add_handler(CommandHandler('groups', list_groups, filters=~Filters.update.edited_message))
    updater.dispatcher.add_handler(CommandHandler('groups_verbose', list_groups_verbose, filters=~Filters.update.edited_message))
    updater.dispatcher.add_handler(CommandHandler('list_members', list_members_short, filters=~Filters.update.edited_message))
    updater.dispatcher.add_handler(CommandHandler('members', list_members_short, filters=~Filters.update.edited_message))
    updater.dispatcher.add_handler(CommandHandler('list_members_verbose', list_members_verbose, filters=~Filters.update.edited_message))
    updater.dispatcher.add_handler(CommandHandler('members_verbose', list_members_verbose, filters=~Filters.update.edited_message))
    updater.dispatcher.add_handler(CommandHandler('join', add_member_current, filters=~Filters.update.edited_message))
    updater.dispatcher.add_handler(CommandHandler('add', add_member_with_id, filters=~Filters.update.edited_message))
    updater.dispatcher.add_handler(CommandHandler('join_id', add_member_with_id, filters=~Filters.update.edited_message))
    updater.dispatcher.add_handler(CommandHandler('remove', remove_member_with_id, filters=~Filters.update.edited_message))
    updater.dispatcher.add_handler(CommandHandler('leave', remove_member_current, filters=~Filters.update.edited_message))
    updater.dispatcher.add_handler(CommandHandler('leave_id', remove_member_with_id, filters=~Filters.update.edited_message))
    updater.dispatcher.add_handler(CommandHandler('ping', ping, filters=~Filters.update.edited_message))
    updater.dispatcher.add_handler(CommandHandler('help', help_message, filters=~Filters.update.edited_message))
    updater.dispatcher.add_handler(CommandHandler('help_verbose', help_message_verbose, filters=~Filters.update.edited_message))
    updater.dispatcher.add_handler(CommandHandler('my_groups', groups_of_which_user_themselves_is_member, filters=~Filters.update.edited_message))
    updater.dispatcher.add_handler(CommandHandler('reveal_groups', groups_of_which_other_user_is_member, filters=~Filters.update.edited_message))
    updater.dispatcher.add_handler(CommandHandler('prune_groups', prune, filters=~Filters.update.edited_message))

    updater.dispatcher.add_handler(MessageHandler(Filters.regex(r'(?:^|\s)@\w+\b'), at_ping))

    updater.start_polling()
